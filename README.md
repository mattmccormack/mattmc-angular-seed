# README

## Installation

Clone this repository for a fresh install of angular

```
git clone https://mattmccormack@bitbucket.org/mattmccormack/mattmc-angular-seed.git
```

To clone without remote origin

```
git init new-app
cd new-app
git pull https://mattmccormack@bitbucket.org/mattmccormack/mattmc-angular-seed.git
```
## Running Python test server

Python 2
```
python -m SimpleHTTPServer
```

Python 3
```
python -m http.server
```

## Git Workflow


pull all the changes from the remote repository
``` 
git pull
```


create a new branch for your bug/feature/issue
```
git checkout -b branch-name
```


**do your work here**
keep it in chunks, the smaller your commits, the better, in case things go wrong


add any new files you've created
```
git add .
```


see the changes you're going to commit
```
git status
```
```
git diff
```


make the commit with a detailed message
```
git commit -m "detailed message here"
```


switch back to the master branch when the feature is done
```
git checkout master
```


update the master branch to update the master with all your changes
```
git merge branch-name-here
```


send your changes up to the remote repository
```
git push
```

### Eg: Update Master branch with Develop branch changes
```
git checkout master
git merge develop
```
### Eg: Update Develop branch with bugfix in Master
```
git checkout develop
git merge master
```
